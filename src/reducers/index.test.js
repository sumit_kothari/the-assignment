import { createStore } from "redux"
import rootReducer from "./index"
import * as actions from "../actions/index"

describe("reducer", () => {
    const base = {
        User: {}
    }

    it("should handle action USER_AUTH", () => {
        const login = false
        const message = "Invalid Email Id or Password"

        expect(
            rootReducer(
                {},
                {
                    type: actions.USER_AUTH,
                    login,
                    message
                }
            )
        ).toEqual(
            Object.assign({}, base, {
                User: {
                    login,
                    message
                }
            })
        )
    })

    it("should handle action REMOVE_LOGIN_ERROR_MESSAGE", () => {
        const message = "Email Id or Password is missing"

        expect(
            rootReducer(
                {},
                {
                    type: actions.REMOVE_LOGIN_ERROR_MESSAGE,
                    message
                }
            )
        ).toEqual(
            Object.assign({}, base, {
                User: {
                    message
                }
            })
        )
    })
})
