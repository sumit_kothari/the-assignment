import { combineReducers } from "redux"

import { USER_AUTH, REMOVE_LOGIN_ERROR_MESSAGE } from "../actions/index"

function User(state = {}, action) {
	switch (action.type) {
		case USER_AUTH:
			return Object.assign({}, state, {
				login: action.login,
				message: action.message
			})
		case REMOVE_LOGIN_ERROR_MESSAGE:
			return Object.assign({}, state, {
				message: action.message
			})
		default:
			return state
	}
}

const rootReducer = combineReducers({ User })

export default rootReducer
