import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";
import AboutUs from "./containers/AboutUs";
import Home from "./containers/Home";
import Login from "./containers/Login";
import NoMatch from "./containers/NoMatch";

const Routes = store => {
    return (
        <Router>
            <Switch>
                <Route exact component={Home} path="/" />
                <Route component={Home} path="/home" />
                <Route component={Login} path="/login" />
                <PrivateRoute
                    store={store}
                    component={AboutUs}
                    path="/aboutus"
                />
                <Route component={NoMatch} />
            </Switch>
        </Router>
    );
};

const PrivateRoute = ({ component: Component, ...rest }) => {
    const state = rest["store"].getState();

    return (
        <Route
            {...rest}
            render={props =>
                state["User"] && state["User"]["login"]
                    ? <Component {...props} />
                    : <Redirect
                          to={{
                              pathname: "/login",
                              state: { from: props.location }
                          }}
                      />}
        />
    );
};

export default Routes;
