import { Users } from "../other/constants/auth-users"

export const USER_AUTH = "USER_AUTH"
export const REMOVE_LOGIN_ERROR_MESSAGE = "REMOVE_LOGIN_ERROR_MESSAGE"

export const user_auth_check = (emailValue, passwordValue) => {
	if (emailValue && passwordValue) {
		if (
			Users[emailValue] &&
			Users[emailValue]["password"] === passwordValue
		) {
			return {
				type: USER_AUTH,
				login: true
			}
		}

		return {
			type: USER_AUTH,
			login: false,
			message: "Invalid Email Id or Password"
		}
	}

	return {
		type: USER_AUTH,
		login: false,
		message: "Email Id or Password is missing"
	}
}

export const user_auth = (login = false, message = "") => {
	return {
		type: USER_AUTH,
		login,
		message
	}
}

export const remove_login_error_message = () => {
	return {
		type: REMOVE_LOGIN_ERROR_MESSAGE,
		message: ""
	}
}
