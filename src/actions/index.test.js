import * as actions from "./index.js"

describe("actions", () => {
	it("should authenticate users sucess", () => {
		const email = "sumit@test.com"
		const password = "123456"

		const expectedAction = {
			type: actions.USER_AUTH,
			login: true
		}

		expect(actions.user_auth_check(email, password)).toEqual(expectedAction)
	})

	it("should authenticate user error", () => {
		const email = "test@test.com"
		const password = "xyz"

		const expectedAction = {
			type: actions.USER_AUTH,
			login: false,
			message: "Invalid Email Id or Password"
		}

		expect(actions.user_auth_check(email, password)).toEqual(expectedAction)
	})

	it("should authenticate user, no email or password provided", () => {
		const email = null
		const password = null

		const expectedAction = {
			type: actions.USER_AUTH,
			login: false,
			message: "Email Id or Password is missing"
		}

		expect(actions.user_auth_check(email, password)).toEqual(expectedAction)
	})

	it("should update authentication state", () => {
		const login = false
		const message = "Any message"

		const expectedAction = {
			type: actions.USER_AUTH,
			login,
			message
		}

		expect(actions.user_auth(login, message)).toEqual(expectedAction)
	})

	it("should remove login error message", () => {
		const expectedAction = {
			type: actions.REMOVE_LOGIN_ERROR_MESSAGE,
			message: ""
		}

		expect(actions.remove_login_error_message()).toEqual(expectedAction)
	})
})
