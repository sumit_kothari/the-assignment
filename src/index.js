import React from "react"
import { render } from "react-dom"
import { Provider } from "react-redux"

import "./other/css/style.css"

import Routes from "./routes"

import configureStore from "./store/index"

let store = configureStore()

render(
	<Provider store={store}>
		{Routes(store)}
	</Provider>,
	document.getElementById("root")
)
