import React, { Component } from "react"

import Header from "./header"

class NoMatch extends Component {
	render() {
		return (
			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">

				<Header screenName="404" noLogout={true} />

				<main className="mdl-layout__content">

					<div className="page-content">

						<div className="mdl-grid">
							<div className="mdl-layout-spacer" />
							<div className="mdl-cell mdl-cell--8-col text-align-right">
								<h3>
									You should go back from where, you will find nothing here; this is Sparta!
								</h3>
							</div>
							<div className="mdl-layout-spacer" />
						</div>
					</div>

				</main>
			</div>
		)
	}
}

export default NoMatch
