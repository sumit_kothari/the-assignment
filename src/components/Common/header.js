import React, { Component } from "react"
import { Link } from "react-router-dom"

class Header extends Component {
	render() {
		const { screenName, noLogout, logoutUser } = this.props

		return (
			<header className="mdl-layout__header">
				<div className="mdl-layout__header-row">
					<Link to="/" className="mdl-layout-title">
						The Assignment
					</Link>
					<span className="mdl-layout-title screen-name-in-header">
						({screenName})
					</span>
					<div className="mdl-layout-spacer" />
					<nav
						className={`mdl-navigation ${noLogout ? "display-none" : ""}`}>
						<a
							className="mdl-navigation__link"
							onClick={logoutUser}>
							Logout
						</a>
					</nav>
				</div>
			</header>
		)
	}
}

export default Header
