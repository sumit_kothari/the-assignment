import React from "react"
import { shallow } from "enzyme"

import NoMatch from "./noMatch"

function setup() {
	const enzymeWrapper = shallow(<NoMatch />)

	return {
		enzymeWrapper
	}
}

describe("components", () => {
	const { enzymeWrapper } = setup()

	describe("noMatch", () => {
		it("should render self and subcomponents", () => {
			expect(enzymeWrapper.find("h3").text()).toBe(
				"You should go back from where, you will find nothing here; this is Sparta!"
			)
		})
	})
})
