import React from "react"
import { shallow } from "enzyme"

import Header from "./header"

function setup() {
	const props = {
		screenName: "Home",
		noLogout: true,
		logoutUser: jest.genMockFunction()
	}

	const enzymeWrapper = shallow(<Header {...props} />)

	return {
		props,
		enzymeWrapper
	}
}

describe("components", () => {
	describe("Header", () => {
		const { enzymeWrapper, props } = setup()

		it("should render self and subcomponents", () => {
			expect(
				enzymeWrapper.find("header").hasClass("mdl-layout__header")
			).toBe(true)

			expect(
				enzymeWrapper
					.find(".mdl-layout-title.screen-name-in-header")
					.text()
			).toBe("(" + props.screenName + ")")
		})

		it("should mock logoutUser function", () => {
			const logoutMethod = enzymeWrapper.find(".mdl-navigation__link")
			logoutMethod.simulate("click")
			expect(props.logoutUser).toBeCalled()
		})
	})
})
