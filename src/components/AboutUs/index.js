import React, { Component } from "react"
import { NavLink, Redirect } from "react-router-dom"

import { Constants } from "../../other/constants/const"

class Home extends Component {
	render() {
		const { match } = this.props
		const { homePageItems } = Constants
		const allowTypes = Object.keys(homePageItems)
		const paramsId = match["params"]["id"]

		if (allowTypes.includes(paramsId)) {
			// activeTabCheck[ match["params"]["id"] ] = true
			// console.log(paramsId);
		} else {
			// console.log("wrong id");
			return <Redirect to="/404" />
		}

		return (
			<main className="mdl-layout__content">

				<div className="mdl-tabs mdl-js-tabs">
					<div className="mdl-tabs__tab-bar">
						<NavLink
							to="/aboutus/profile"
							activeClassName="my-tab-active"
							className="mdl-tabs__tab">
							{" "}Profile{" "}
						</NavLink>
						<NavLink
							to="/aboutus/team"
							activeClassName="my-tab-active"
							className="mdl-tabs__tab ">
							{" "}Team{" "}
						</NavLink>
						<NavLink
							to="/aboutus/contact"
							activeClassName="my-tab-active"
							className="mdl-tabs__tab ">
							{" "}Contact{" "}
						</NavLink>
					</div>
				</div>

				<div className="page-content">

					<div className="mdl-grid">
						<div className="mdl-layout-spacer" />
						<div className="mdl-cell mdl-cell--8-col">
							<h3>{homePageItems[paramsId]["description"]}</h3>
						</div>
						<div className="mdl-layout-spacer" />
					</div>

				</div>
			</main>
		)
	}
}

export default Home
