import React, { Component } from "react"

import Header from "../Common/header"

class Home extends Component {
	handleSubmit(event, authUser) {
		event.preventDefault()

		const emailValue = this.refs.email.value.trim()
		const passwordValue = this.refs.password.value.trim()

		if (emailValue && passwordValue) {
			authUser(emailValue, passwordValue)
		}
	}

	render() {
		const { authUser, errorMessage } = this.props

		return (
			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">

				<Header screenName="Login" noLogout={true} />

				<main className="mdl-layout__content">

					<div className="page-content">

						<div className="mdl-grid">
							<div className="mdl-layout-spacer" />
							<div className="mdl-cell mdl-cell--6-col text-align-right">
								<form
									id="myForm"
									onSubmit={e => {
										this.handleSubmit(e, authUser)
									}}>

									<div className="errorMessage">
										{errorMessage}
									</div>

									<div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input
											ref="email"
											className="mdl-textfield__input"
											type="text"
											pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"
											id="email-text-field"
										/>
										<label
											className="mdl-textfield__label"
											htmlFor="email-text-field">
											Email Id
										</label>
										<span className="mdl-textfield__error">
											Hey Stranger, a valid Email Id is required!
										</span>
									</div>
									<div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input
											ref="password"
											className="mdl-textfield__input"
											type="password"
											id="password-text-field"
										/>
										<label
											className="mdl-textfield__label"
											htmlFor="password-text-field">
											Password
										</label>
									</div>

									<button
										type="submit"
										className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored margin-top-10">
										Login
									</button>
								</form>
							</div>
							<div className="mdl-layout-spacer" />
						</div>
					</div>

				</main>

			</div>
		)
	}
}

export default Home
