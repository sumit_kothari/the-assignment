import React from "react"
import { shallow } from "enzyme"

import CardItem from "./cardItem"

function setup() {
	const props = {
		itemDetail: {
			title: "Test",
			description: "any",
			linkHref: "/test",
			link: "test"
		}
	}

	const enzymeWrapper = shallow(<CardItem {...props} />)

	return {
		props,
		enzymeWrapper
	}
}

describe("components", () => {
	const { enzymeWrapper, props } = setup()

	describe("noMatch", () => {
		it("should render self and subcomponents", () => {
			expect(enzymeWrapper.find("h2").text()).toBe(props.itemDetail.title)

			expect(
				enzymeWrapper.find(".mdl-card__supporting-text").text()
			).toBe(props.itemDetail.description)
		})
	})
})
