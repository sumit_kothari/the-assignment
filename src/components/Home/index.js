import React, { Component } from "react"

import Header from "../Common/header"
import CardItem from "./cardItem"

class Home extends Component {
	render() {
		const { homePageItems, logoutUser } = this.props

		return (
			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">

				<Header
					screenName="Home"
					logoutUser={logoutUser}
					noLogout={true}
				/>

				<main className="mdl-layout__content">

					<div className="mdl-grid">
						<div className="mdl-layout-spacer" />
						<div className="mdl-cell mdl-cell--9-col">

							<div className="page-content custom-flex-container">

								<CardItem
									itemDetail={homePageItems["profile"]}
								/>
								<CardItem itemDetail={homePageItems["team"]} />
								<CardItem
									itemDetail={homePageItems["contact"]}
								/>

							</div>

						</div>
						<div className="mdl-layout-spacer" />
					</div>

				</main>

			</div>
		)
	}
}

export default Home
