import React, { Component } from "react"
import { Link } from "react-router-dom"

class CardItem extends Component {
	render() {
		const itemDetail = this.props.itemDetail

		return (
			<div className="demo-card-wide mdl-card mdl-shadow--2dp custom-flex-item">
				<div className="mdl-card__title">
					<h2 className="mdl-card__title-text">
						{itemDetail["title"]}
					</h2>
				</div>
				<div className="mdl-card__supporting-text">
					{itemDetail["description"]}
				</div>
				<div className="mdl-card__actions mdl-card--border">
					<Link
						to={itemDetail["linkHref"]}
						className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"
						replace>
						{itemDetail["link"]}
					</Link>
				</div>
			</div>
		)
	}
}

export default CardItem
