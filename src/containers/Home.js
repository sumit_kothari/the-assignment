import React, { Component } from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"

import { user_auth } from "../actions/index"

import { Constants } from "../other/constants/const"

import { default as HomePage } from "../components/Home/index"

class Home extends Component {
	render() {
		const { homePageItems } = Constants
		const { logoutUser } = this.props

		return (
			<HomePage homePageItems={homePageItems} logoutUser={logoutUser} />
		)
	}
}

const mapStateToProps = (state, ownProps) => {
	return state
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		logoutUser: () => {
			dispatch(user_auth(false))
			ownProps.history.push("/login")
		}
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home))
