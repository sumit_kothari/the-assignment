import React, { Component } from "react"
import { connect } from "react-redux"
import { Redirect, withRouter } from "react-router-dom"

import { user_auth_check, remove_login_error_message } from "../actions/index"

import { default as LoginPage } from "../components/Login/index"

class Login extends Component {
	componentDidMount() {
		const { removeErrorMessage } = this.props
		removeErrorMessage()
	}

	render() {
		if (this.props["User"] && this.props["User"]["login"]) {
			return <Redirect to="/home" />
		}

		const { authUser, errorMessage } = this.props

		return <LoginPage authUser={authUser} errorMessage={errorMessage} />
	}
}

const mapStateToProps = (state, ownProps) => {
	if (state["User"] && state["User"]["login"] === false) {
		return { errorMessage: state["User"]["message"] }
	}

	return state
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		authUser: (emailValue, passwordValue) => {
			dispatch(user_auth_check(emailValue, passwordValue))
		},
		removeErrorMessage: () => {
			dispatch(remove_login_error_message())
		}
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))
