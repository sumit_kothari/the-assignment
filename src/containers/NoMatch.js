import React, { Component } from "react"
import { connect } from "react-redux"

import { default as NoMatchPage } from "../components/Common/noMatch"

class NoMatch extends Component {
	render() {
		return <NoMatchPage />
	}
}

const mapStateToProps = (state, ownProps) => {
	return state
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(NoMatch)
