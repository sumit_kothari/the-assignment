import React, { Component } from "react"
import { connect } from "react-redux"
import { Route, Redirect, withRouter } from "react-router-dom"
import { user_auth } from "../actions/index"

import { default as AboutUsPage } from "../components/AboutUs/index"

import Header from "../components/Common/header"

class AboutUs extends Component {
	render() {
		const { match } = this.props
		const { logoutUser } = this.props

		if (match["isExact"]) {
			return <Redirect to={`${match.url}/profile`} />
		}

		return (
			<div>
				<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">

					<Header screenName="AboutUs" logoutUser={logoutUser} />

					<PropsRoute
						path={`${match.url}/:id`}
						component={AboutUsPage}
						logoutUser={logoutUser}
					/>

				</div>
			</div>
		)
	}
}

const renderMergedProps = (component, ...rest) => {
	const finalProps = Object.assign({}, ...rest)
	return React.createElement(component, finalProps)
}

const PropsRoute = ({ component, ...rest }) => {
	return (
		<Route
			{...rest}
			render={routeProps => {
				return renderMergedProps(component, routeProps, rest)
			}}
		/>
	)
}

const mapStateToProps = (state, ownProps) => {
	return state
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		logoutUser: () => {
			dispatch(user_auth(false))
			ownProps.history.push("/login")
		}
	}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutUs))
