export const Constants = {
	homePageItems: {
		profile: {
			title: "Profile",
			description: "The profile you are about to visit is constantly monitored by profile owner, hence you see only the things profile owner wants you to see ;)",
			link: "Go to Profile",
			linkHref: "aboutus/profile"
		},
		team: {
			title: "Team",
			description: "This team consist of people who play for same goal, sometimes the goal seems easy but the goalkeeper is Oliver Kahn!",
			link: "View Team",
			linkHref: "aboutus/team"
		},
		contact: {
			title: "Contact",
			description: "Warning: When you contact us, we contact you back and this contact will echo throughout the Galaxy; maybe some aliens will try to stop us but be assured, this contact will be made.",
			link: "Contact Us Now",
			linkHref: "aboutus/contact"
		}
	}
}
