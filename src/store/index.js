import { createStore } from "redux"
import rootReducer from "../reducers/index"

export default function configureStore(preloadedState) {
	// resux state is store in session storage to properly maintain authentication
	const persistedState = sessionStorage.getItem("reduxState")
		? JSON.parse(sessionStorage.getItem("reduxState"))
		: {}

	const store = createStore(rootReducer, persistedState)

	store.subscribe(() => {
		sessionStorage.setItem("reduxState", JSON.stringify(store.getState()))
	})

	return store
}
