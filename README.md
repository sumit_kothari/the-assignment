# The Assignment

Live Url: https://the-assignment-task.herokuapp.com/

# Prerequest

  - Node: v6.1.0
  - npm: 3.8.6

# Usage

  - Clone this repo and cd to repo's root
  - `npm install` : to install packages and dependencies
  - `npm start` : to run app in local environment
  - `npm run test` : to run unit test cases